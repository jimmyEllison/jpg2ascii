from PIL import Image, ImageDraw
import math
import os.path
import sys

SEQUENCE = ' .:-=+*#%@'
SEQUENCE_LEN = len(SEQUENCE)
CHAR_WIDTH = 8
CHAR_HEIGHT = 16


def get_char(inputInt):
    return SEQUENCE[math.floor(inputInt * (SEQUENCE_LEN / 256))]


def resize(image, new_width=448):
    width, height = image.size
    ratio = height / width
    new_height = int(new_width * ratio)
    resize_img = image.resize(
        (new_width, int(new_height * (CHAR_WIDTH / CHAR_HEIGHT))),
        Image.Resampling.LANCZOS)
    return resize_img


def grayify(image):
    grayified_img = image.convert('L')
    return grayified_img

def pixel_to_ascii(image):
    pixels = image.getdata()
    return ''.join([get_char(pixel) for pixel in pixels])


def main(new_width=448):
    try:
        path = sys.argv[1]
        image = Image.open(path)
    except Exception as err:
        print(err)

    name, ext = os.path.splitext(path)
    output = '{}_edited.txt'.format(name)

    grayified_img = grayify(resize(image))
    data = pixel_to_ascii(grayified_img)
    data_length = len(data)
    ascii_img = '\n'.join([data[i:i+new_width] for i in range(0, data_length, new_width)])

    with open('{}'.format(output), 'w') as wf:
        wf.write(ascii_img)


if __name__ == '__main__':
    main()
