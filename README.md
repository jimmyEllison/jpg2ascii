# jpeg2ascii

### Overview
A simple python script that convert image to ascii representation

### Reqirements
- Latest version of Python along with pip
- Python PIL library

### How to run
- Install the necessary requirements by issuing the following command:
`pip install -r requirements.txt`
- In the terminal, type in `python main path/to/image` then check directory for newly created text file

### Bugs
Just haven't found it yet!

### License
BSD 2-Clause License

